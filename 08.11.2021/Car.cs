﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _08._11._2021
{
    class Car
    {
        private int _speed;
        private int _leng;
        public int Speed 
        {
            get
            {
                return _speed;
            } 
            set
            {
                _speed = value;   
            } 
        }
        public string Model { get; set; }
        public int CountDoors { get; set; }
        public Car(int speed, string model, int countdoors)
        {
            Speed = speed;
            CountDoors = countdoors;
            Model = model;
        }
        public string Signal()
        {
            return "PICK-PICK";
        }
        public void Run(int leng)
        {
            _leng = leng +_leng;
        }
        public int GetAllLeng()
        {
            return _leng;
        }
    }
}
