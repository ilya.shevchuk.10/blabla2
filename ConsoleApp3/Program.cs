﻿using System;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] MyArray = GetRandomArray(10,0,23);
            foreach (var item in MyArray)
            {
                Console.Write(item+"\t");
            }
            int x = 15;
           int rez= IndexOf(MyArray, x);
            Console.WriteLine(rez);
        }
        static int[] GetRandomArray(uint lent, int minValy, int maxValy)
        {
            int [] myArray= new int [lent];
            Random random = new Random();
            for (int i = 0; i < lent; i++)
            {
                myArray[i] = random.Next(minValy, maxValy);
            }
            return myArray;
        }
        static int IndexOf(int [] Array, int valye )
        {
            
            for (int i = 0; i < Array.Length; i++)
            {
                if (Array[i] == valye)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
