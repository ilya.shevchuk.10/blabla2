﻿using System;

namespace ConsoleApp4
{
    class Program
    {
        //static int Sum(string name,params int[] p)
        //{
        //    int rez = 0;
        //    for (int i = 0; i < p.Length; i++)
        //    {
        //        rez += p[i];
        //    }
        //    return rez;
        //}
        //static void Foo(params object[] parametrs)
        //{
        //    string masege = "Tup danuk {0}, znachenya {1} ";
        //    foreach (var item in parametrs)
        //    {
        //        Console.WriteLine(masege, item.GetType(), item);
        //    }
        //}
        static void Foo(int [] MyArry, int i=0)
        {
            if (i < MyArry.Length)
            {
                Console.WriteLine(MyArry[i]);
                Foo(MyArry, i + 1);
            }
           
        }
        static int Sum(int[] Arry, int i = 0)
        {
            if (i >= Arry.Length)
                return 0;
            int rezult = Sum(Arry, i + 1);
                return rezult + Arry[i];
        }
        static int SumChus(int value)
        {
            if (value < 10)
                return value;
            int digit = value % 10;
            int maxValue = value / 10;
            return digit + SumChus(maxValue);
            
        }
        static void Main(string[] args)
        {
            int[] arry = new int [] { 1, 2, 3, 4, 5 };
            Foo(arry);
            Console.WriteLine(Sum(arry));
            int ValueT = 1998;
            int rez=SumChus(ValueT);
            
        }
    }
}
