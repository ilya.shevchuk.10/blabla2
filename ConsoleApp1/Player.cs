﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Player
    {
        public string name;
        public Cart[] carts;
        public int value;
        public bool inGame;
        public Player(string name,DeckOfCards deckOfCards)
        {
            this.name = name;
            SetFirstCarts(deckOfCards);
            inGame = true;
        }
        public void SetFirstCarts(DeckOfCards first)
        {
            // Cart[] firstcart = new Cart[1] {new Cart(6,Suit.Clubs)};
            carts = new Cart[1] {first.OutCartOfDeck()};
        }
        public void isGame(DeckOfCards deckOfCards)
        {
            int rez = 0;
            for (int i = 0; i < carts.Length; i++)
            {
                rez += carts[i].value;
          
            }
            value += rez;
            if (value < 16)
            {
                Cart [] temp = new Cart[carts.Length+1];
                for (int i = 0; i < temp.Length; i++)
                {
                    temp[i] = carts[i];
                }
                carts = temp;
                temp[temp.Length - 1] = deckOfCards.OutCartOfDeck() ;
            }
            if (value > 16)
            {
                inGame = false;
            }
        }
        private Cart CartGetInColoda(DeckOfCards deckOfCards)
        {
            return deckOfCards.OutCartOfDeck();
        }
    }
}
