﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Cart
    {
        public int value;
        public Suit suit;
        public Cart( int value, Suit suit)
        {
            this.value = value;
            this.suit = suit;
        }


    }
    public enum Suit
    {
        Clubs,
        Diamonds,
        Hearts,
        Spades,
    }
}
