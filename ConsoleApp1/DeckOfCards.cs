﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class DeckOfCards
    {
        public Cart[] carts;
        public DeckOfCards()
        {
            SetDeckOfCarts();
        }
        private void SetDeckOfCarts()
        {
            carts = new Cart[36];

            carts[0] = new Cart(6, Suit.Clubs);
            carts[1] = new Cart(6, Suit.Clubs);
            carts[2] = new Cart(6, Suit.Clubs);
            carts[3] = new Cart(6, Suit.Clubs);
            carts[4] = new Cart(7, Suit.Clubs);
            carts[5] = new Cart(7, Suit.Clubs);
            carts[6] = new Cart(7, Suit.Clubs);
            carts[7] = new Cart(7, Suit.Clubs);
            carts[8] = new Cart(8, Suit.Clubs);
            carts[9] = new Cart(8, Suit.Clubs);
            carts[10] = new Cart(8, Suit.Clubs);
            carts[11] = new Cart(8, Suit.Clubs);
            carts[12] = new Cart(9, Suit.Clubs);
            carts[13] = new Cart(9, Suit.Clubs);
            carts[14] = new Cart(9, Suit.Clubs);
            carts[15] = new Cart(9, Suit.Clubs);
            carts[16] = new Cart(10, Suit.Clubs);
            carts[17] = new Cart(10, Suit.Clubs);
            carts[18] = new Cart(10, Suit.Clubs);
            carts[19] = new Cart(10, Suit.Clubs);
            carts[20] = new Cart(2, Suit.Clubs);
            carts[21] = new Cart(2, Suit.Clubs);
            carts[22] = new Cart(2, Suit.Clubs);
            carts[23] = new Cart(2, Suit.Clubs);
            carts[24] = new Cart(3, Suit.Clubs);
            carts[25] = new Cart(3, Suit.Clubs);
            carts[26] = new Cart(3, Suit.Clubs);
            carts[27] = new Cart(3, Suit.Clubs);
            carts[28] = new Cart(4, Suit.Clubs);
            carts[29] = new Cart(4, Suit.Clubs);
            carts[30] = new Cart(4, Suit.Clubs);
            carts[31] = new Cart(4, Suit.Clubs);
            carts[32] = new Cart(11, Suit.Clubs);
            carts[33] = new Cart(11, Suit.Clubs);
            carts[34] = new Cart(11, Suit.Clubs);
            carts[35] = new Cart(11, Suit.Clubs);
            Mix();

        }
        public void Mix()
        {
            Random random = new Random();
            for (int i = 0; i < 36; i++)
            {
                int a = random.Next(0, 35);
                int b = random.Next(0, 35);
                Cart temp = carts[a];
                carts[a] = carts[b];
                carts[b] = temp;
            }
        }
        public Cart OutCartOfDeck()
        {
            Cart temp = carts[0];
            Cart[] ubdatecoloda = new Cart[carts.Length - 1];
            for (int i = 0; i < carts.Length - 1; i++)
            {
                ubdatecoloda[i] = carts[i + 1];
            }
            carts = ubdatecoloda;
            return temp;
        }
    }



}

