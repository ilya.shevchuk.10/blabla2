﻿using System;

namespace LOGISTIK
{
    class Program
    {

        static void Main(string[] args)
        {
            var sea = new SeaLigistik();
            var truk = new RoadLogistik();
            var car=GeneraitetRANSPORT(truk);
            var ship = GeneraitetRANSPORT(sea);
            Console.WriteLine(car.name);
            Console.WriteLine(ship.name);

        }
        static Transport GeneraitetRANSPORT(Ilogistik logistik)
        {
            return logistik.GetTransport();
        }
        
    }
    interface Ilogistik
    {
        Transport GetTransport();
    }
    
    class SeaLigistik : Ilogistik
    {
        public Transport GetTransport()
        {
            var  t = new Transport();
            t.name = "ship";
            return t;
        }
    }
    class RoadLogistik : Ilogistik
    {
        public Transport GetTransport()
        {
            var r=new Transport();
            r.name = "truk";
            return r;
        }
    }

    class Transport
    {
        public string name;

    }
}

