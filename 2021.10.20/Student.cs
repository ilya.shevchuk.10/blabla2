﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2021._10._20
{
    class Student
    {
        public Guid id;
        public string firstName;
        public string middleName;
        public string lastName;
        public int age;
        public string group;

        public void Print()
        {
            Console.WriteLine($"Imya {middleName}");
            Console.WriteLine($"Familiya {firstName}");
            Console.WriteLine($" {id}");
            Console.WriteLine($" {lastName}");
            Console.WriteLine($" {age}");
            Console.WriteLine($" {group}");
            Console.WriteLine();
        }
        public string GetFullName()
        {
            return $"Imya {middleName}, Familiya {firstName}, O4estvo {lastName}";
        }
  
    }
}
